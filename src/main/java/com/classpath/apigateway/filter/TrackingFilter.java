package com.classpath.apigateway.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;

@Component
@Slf4j
public class TrackingFilter implements GlobalFilter {
    private static final String  CORRELATION_ID ="CORRELATION_ID";
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info(" Came inside the filter method of API Gateway :: ");
        final List<String> correlationIds = exchange.getRequest().getHeaders().get(CORRELATION_ID);
        if (correlationIds == null || correlationIds.isEmpty()){
            final String correlationId = generateCorrelationId();
            log.info("Generating a correlation id and injecting to the request header :: {}", correlationId);
            exchange.getRequest().mutate().header(CORRELATION_ID, correlationId);
        }
        return chain.filter(exchange);
    }

    private String generateCorrelationId() {
        return UUID.randomUUID().toString();
    }
}